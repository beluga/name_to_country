import logging

class classproperty(object):
    def __init__(self, fget):
        self.fget = fget

    def __get__(self, owner_self, owner_cls):
        return self.fget(owner_cls)


class LogMixin(object):
    # @property
    # def logger(self):
    #     print("ins")
    #     name = ".".join([__name__, self.__class__.__name__])
    #     return logging.getLogger(name)

    @classproperty
    def logger(cls):
        name = cls.__name__
        return logging.getLogger(name)
