import logging
import subprocess
import os
import hashlib
import collections
import json
import numpy as np
import pandas as pd
from .logmix import LogMixin


class OlympicAthletes(LogMixin):

    def __init__(self, partition={"train": 0.8, "test": 0.2}, lockfile=None):
        """
        Initialize olympic athlete database and generate batch data using
        `batch_generator`.
        :param partition: dictionary from label to fraction of the total sample.
        :param lockfile: if not None and the file exists, use the lockfile to assign
                         rows to label according to lockfile.
                         if not None but the file does not exist, use the partition
                         parameter to partition the sample, and save the row numbers in
                         the lockfile.
                         By default, None.
        Note either `partition` or `lockfile` should be None.
        """
        lockfile_available = lockfile is not None and os.path.exists(lockfile)
        assert partition is not None or lockfile_available
        assert partition is None or not lockfile_available
        db = self._fetch_olympic_athlete_db()
        if lockfile_available:
            self.logger.debug("lockfile {} exists to retrieve row indices for train/test/etc.".format(lockfile))
            with open(lockfile) as j:
                partition_with_ids = json.load(j)
        else:
            self.logger.debug("lockfile is not found. create row indices for train/test/etc.")
            lendb = len(db)
            indices = np.arange(lendb)
            np.random.shuffle(indices)
            partition_pairs = partition.items()
            # add a dummy label in partition, for the remainders.
            partition["__remaining__"] = 1.0 - sum(v for k,v in partition_pairs)
            positions = np.cumsum([int(v*lendb) for _,v in partition_pairs][:-1])
            split_indices = (x.tolist() for x in np.split(indices, positions))
            partition_with_ids = dict(zip((k for k,_ in partition_pairs), split_indices))
            if lockfile is not None:
                self.logger.debug("save row indices for tra/test/etc in {}".format(lockfile))
                # save the ids in the lockfile.
                with open(lockfile, "w") as o:
                    json.dump(partition_with_ids, o)

        self._partition_with_ids = {k:np.array(v) for k,v in partition_with_ids.items()}
        self.logger.debug("number of rows: {}".format({k:len(v) for k,v in partition_with_ids.items()}))

        (self._name_indices,
         self._name_char_list,
         self._country_indices,
         self._country_list,
         self._name,
         self._country) = self._process_olympic_athlete_db(db)
        self._index_dict = {k:0 for k,_ in partition_with_ids.items()}
        self._totallen_dict = {k:len(v) for k, v in partition_with_ids.items()}

    @property
    def name_char_list(self):
        return self._name_char_list

    @property
    def country_list(self):
        return self._country_list

    @property
    def name(self):
        return self._name

    @property
    def country(self):
        return self._country

    def generate_batch(self, label, batch_size):
        """
        generate a batch of size batch_size (whole if batch_size=None).
        :param label: a label representing a portion of the whole data (train/test/etc).
        :param batch_size: the number of rows to extract. whole rows if None.
        :return: name_matrix (batch_size x maximum char length, 0 padded) for
                 character indices, lengths for name lengths, and country_batch
                 for the country indices (vector of length batch_size).
        """
        total_len = self._totallen_dict[label]
        batch_size = total_len if batch_size is None else batch_size
        assert batch_size <= total_len
        if self._index_dict[label] + batch_size > total_len:
            self.logger.debug("reshuffling...")
            np.random.shuffle(self._partition_with_ids[label])
            self._index_dict[label] = 0

        ids = self._partition_with_ids[label]
        ind = self._index_dict[label]
        name_batch = self._name_indices[ids[ind:ind+batch_size]]
        country_batch = self._country_indices[ids[ind:ind+batch_size]]
        lengths = np.vectorize(len)(name_batch)
        max_length = lengths.max(axis=0)
        padded = (np.pad(x, (0,max_length-len(x)),
                             mode="constant",
                             constant_values=(np.int32(0),np.int32(0))) for x in name_batch)
        name_matrix = np.row_stack(padded)
        #self._index_dict[label] = (ind + batch_size) % total_len
        self._index_dict[label] += batch_size
        return name_matrix, lengths, country_batch

    def batch_generator(self, label, batch_size):
        while True:
            yield self.generate_batch(label, batch_size)

    def string_generate_batch(self, label, batch_size):
        """
        generate a batch of size batch_size (whole if batch_size=None) of
        namd and country vectors.
        :param label: a label representing a portion of the whole data (train/test/etc).
        :param batch_size: the number of rows to extract. whole rows if None.
        :return: name string vector, country string vector.
        """
        total_len = self._totallen_dict[label]
        batch_size = total_len if batch_size is None else batch_size
        assert batch_size <= total_len
        if self._index_dict[label] + batch_size > total_len:
            self.logger.debug("reshuffling...")
            np.random.shuffle(self._partition_with_ids[label])
            self._index_dict[label] = 0

        ids = self._partition_with_ids[label]
        ind = self._index_dict[label]
        name_batch = self.name[ids[ind:ind+batch_size]]
        country_batch = self.country[ids[ind:ind+batch_size]]
        return collections.namedtuple("string_batch", ["name", "country"])(name_batch, country_batch)

    def string_batch_generator(self, label, batch_size):
        """ generator for name and country string vectors """
        while True:
            yield self.string_generate_batch(label, batch_size)

    @classmethod
    def _fetch_olympic_athlete_db(cls, force=False):
        """
        Fetch olympic athlete name/country/dob/gender database.
        The ascii version expands the names and birth places into only ascii characters, in case
        the original ones have unicode characters.
        """
        ascii_url = "https://www.dropbox.com/sh/3p3txwpbl87y77v/AAA_AirKwipnjnst0R240FPba/olympic_athletes_ascii.csv?dl=0"
        orig_url = "https://www.dropbox.com/sh/3p3txwpbl87y77v/AADsVrq8WLSsmsUcuWvvK32Aa/olympic_athletes.csv?dl=0"
        url = ascii_url
        tempfile = "/tmp/olympic_athlete_temp.{}.csv".format(hashlib.md5(url.encode()).hexdigest())
        if os.path.isfile(tempfile) and not force:
            cls.logger.debug("file {} already exists for url {}".format(tempfile, url))
        else:
            cls.logger.debug("reading the database in {} and saving it into {}".format(url, tempfile))
            subprocess.call(["curl", "-L", url, "-o", tempfile, "-s"])
        tbl = pd.read_csv(tempfile)
        tbl = tbl.assign(name=np.char.strip(np.char.translate(np.char.lower(tbl.ascii_name.values.astype("U")), {
            ord("."): "",
            ord("'"): "",
            ord(","): "",
            ord("&"): "",
            ord(")"): "",
            ord("("): "",
            ord("\\"): "",
            ord("`"): "",
            ord("-"): " ",
            ord("@"): "a",
        })))
        # tbl = tbl.assign(name=tbl.ascii_name)
        # check if name characters are alphabets+space.
        cls.logger.debug("character occurrences in the name column: {}".format(
            collections.Counter("".join(tbl.name)).most_common(100)))

        return tbl

    @classmethod
    def _process_olympic_athlete_db(cls, db):
        """
        extract relavant information from the olympic athlete database
        :param db: the olympic athlete database from fetch_olympic_athlete_db
        :return: an index vector of vectors for names, a vector as index -> character for name vector characters,
                 an index vector for countries, a vector as index -> country.
        """
        name_chars = "abcdefghijklmnopqrstuvwxyz "
        name_dict = dict(zip(name_chars, range(len(name_chars))))
        name_category = db["name"].apply(lambda s: np.fromiter((name_dict[x] for x in s), dtype=np.int32))
        #country_category = db["country"].astype("category")
        countries = [x for x,_ in collections.Counter(db["country"]).most_common()]
        country_dict = dict(zip(countries, range(len(countries))))
        country_category = db["country"].apply(lambda s: np.int32(country_dict[s]))
        return (name_category, name_chars,
                country_category, countries,
                db.name, db.country)


