import numpy as np
import editdistance

class EditDistance(object):
    def __init__(self, names, countries):
        self._names = names
        self._countries = countries

    def validate(self, train_batch_pair, test_batch_pair, num_guesses=5):
        """
        For each name in `test_batch_pair`, find the `num_guesses`
        distinct closest names in `train_batch_pair`.
        """
        train_batch_names = train_batch_pair.name.values
        train_batch_countries = train_batch_pair.country.values
        result = []
        for step, (name, country) in enumerate(zip(*test_batch_pair)):
            distances = np.vectorize(lambda x: editdistance.eval(name, x))(train_batch_names)
            distincts = take_distinct(num_guesses, train_batch_countries[np.argsort(distances)])

            result.append(country in distincts)
            print("\rstep {}, correct ratio: {:.5f}".format(step, sum(result) / len(result)), end="")

        return result

def take_distinct(n, vs):
    """
    Take the first `n` distinct elements from `vs`, without altering the order.
    If there are fewer than `n` distinct elements in `vs`, return as many as possible.
    """
    r = []
    for v in vs:
        if len(r) >=n: return r
        if not v in r: r.append(v)
    return r
