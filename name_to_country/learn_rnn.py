import numpy as np
import os
import collections
import itertools
import tensorflow as tf
from .logmix import LogMixin


class LearnRnn(LogMixin):
    def __init__(self,
                 num_rnn_cells,
                 num_output_layers,
                 char_embedding_dims,
                 country_embedding_dims,
                 output_keep_prob=1.0,
                 graph=None,
                 use_nce=False,
                 scope="learnrnn"):
        """
        Create a RNN for name to country mapping.

        :param num_rnn_cells: RNN cell sizes.
        :param num_output_layers: Output layer that connects the final output from RNN to the country embedding.
                                  [] means directly performing dot product between the output of RNN to the
                                  country embedding, meaning their dimensions have to match.
                                  In general, the last component of num_output_layers should match
                                  the dimensions of the country embedding.
        :param char_embedding_dims: a matrix dimensions of the form (27, integer),
                                    to create a matrix for character embedding.
        :param country_embedding_dims: a matrix dimensions of the form (num of countries, integer),
                                       to create a matrix for country embedding.
        """
        self._num_rnn_cells = num_rnn_cells
        self._num_output_layers = num_output_layers
        self._char_embedding_dims = char_embedding_dims
        self._country_embedding_dims = country_embedding_dims
        self._num_sampled = 64
        self._name_char_list = None
        self._country_list = None
        self._output_keep_prob = output_keep_prob

        self._graph = graph or tf.get_default_graph()
        self._scope = scope
        self._use_nce = use_nce

        self._model = self._build_graph(num_rnn_cells,
                                        num_output_layers,
                                        char_embedding_dims,
                                        country_embedding_dims)

    @property
    def model(self):
        return self._model

    def _build_graph(self, num_rnn_cells, num_output_layers, char_embedding_dims, country_embedding_dims):
        with self._graph.as_default(), \
             tf.variable_scope(self._scope):
            input_name_chars = tf.placeholder(tf.int32, [None, None])
            input_name_lengths = tf.placeholder(tf.int32, [None])
            input_countries = tf.placeholder(tf.int32, [None])

            with tf.variable_scope("rnn"):
                name_embeddings = tf.get_variable("name", char_embedding_dims,
                                                  dtype=tf.float32,
                                                  initializer=tf.random_uniform_initializer(-1.0, 1.0))

                name_embedded = tf.gather(name_embeddings, input_name_chars)
                # country_embedded = tf.gather(country_embeddings, input_countries)
                output_keep_prob = itertools.repeat(self._output_keep_prob) \
                    if check_float(self._output_keep_prob) \
                    else self._output_keep_prob
                cells = [tf.nn.rnn_cell.DropoutWrapper(tf.nn.rnn_cell.LSTMCell(n),
                                                       output_keep_prob=p)
                         for n, p in zip(num_rnn_cells, output_keep_prob)]
                cell = tf.nn.rnn_cell.MultiRNNCell(cells=cells)

                output, _ = tf.nn.dynamic_rnn(
                    cell, name_embedded, dtype=tf.float32, sequence_length=input_name_lengths
                )

                # need to get the last relevant snapshot of output.
                output = self._last_relevant(output, input_name_lengths)

                prev_size = num_rnn_cells[-1]
                for i, n in enumerate(num_output_layers):
                    output_weights = tf.get_variable("rnn_out_weight_{}".format(i),
                                                     (prev_size, n),
                                                     dtype=tf.float32,
                                                     initializer=tf.contrib.layers.xavier_initializer())
                    output_biases = tf.get_variable("rnn_out_bias_{}".format(i),
                                                    n,
                                                    dtype=tf.float32,
                                                    initializer=tf.constant_initializer(0.0))
                    output = tf.nn.bias_add(tf.matmul(output, output_weights), output_biases)
                    prev_size = n

            with tf.variable_scope("logit"):
                country_weights = tf.get_variable("country_weight", country_embedding_dims,
                                                  dtype=tf.float32,
                                                  initializer=tf.contrib.layers.xavier_initializer())
                country_biases = tf.get_variable("country_bias", country_embedding_dims[0],
                                                 dtype=tf.float32,
                                                 initializer=tf.constant_initializer(0.0))
                logits = tf.nn.bias_add(tf.matmul(output, country_weights, transpose_b=True),
                                        country_biases)
                inference = tf.nn.softmax(logits)

            with tf.variable_scope("loss"):
                if self._use_nce:
                    self.logger.debug("using nce_loss function...")
                    loss = tf.reduce_mean(
                        tf.nn.nce_loss(weights=country_weights,
                                       biases=country_biases,
                                       labels=tf.reshape(input_countries, shape=tf.pack([tf.shape(input_countries)[0], 1])),
                                       inputs=output,
                                       num_sampled=self._num_sampled,
                                       num_classes=country_embedding_dims[0])
                    )
                else:
                    self.logger.debug("using softmax cross entropy loss function...")
                    loss = tf.reduce_mean(
                        tf.nn.sparse_softmax_cross_entropy_with_logits(
                            logits=logits,
                            labels=input_countries,
                        )
                    )

            result = collections.namedtuple("GraphInOut", ["ins", "outs"])

            return result(
                ins=collections.namedtuple(
                    "ins",
                    ["input_name_chars", "input_name_lengths", "input_countries"]) \
                    (input_name_chars, input_name_lengths, input_countries),
                outs=collections.namedtuple(
                    "outs",
                    ["output", "inference", "loss",
                     "country_weights", "country_biases"])(output, inference, loss,
                                                           country_weights, country_biases)
            )

    @classmethod
    def _last_relevant(cls, output, input_name_lengths):
        batch_size = tf.shape(output)[0]
        max_length = tf.shape(output)[1]
        index = tf.range(0, batch_size) * max_length + (input_name_lengths - 1)
        flat = tf.reshape(output, tf.pack([batch_size * max_length] + \
                                          tf.unpack(tf.shape(output))[2:]))
        relevant = tf.gather(flat, index)
        return relevant

    def best_guess(self, inference, num_guesses=10):
        """
        list the best `num_guesses` guesses based on the inference results of names.
        """
        if self._country_list is None:
            self.logger.error("country list does not exist yet to map countries to integers. Run train method first.")
            raise ValueError("country_list is None.")

        argsorted = (np.argsort(s)[-num_guesses:][::-1] for s in inference)
        return [[self._country_list[x] for x in s] for s in argsorted]

    def inference(self, names, name_char_list=None, country_list=None, sess=None, modelfile=None):

        # either sess or modelfile (but not both) are provided.
        assert sess is None or modelfile is None
        assert sess is not None or modelfile is not None

        def subfunc(sess):
            # validation purpose if not None.
            if name_char_list is not None:
                if self._name_char_list is None:
                    self._name_char_list = name_char_list
                else:
                    assert self._name_char_list == name_char_list
            if country_list is not None:
                if self._country_list is None:
                    self._country_list = country_list
                else:
                    assert self._country_list == country_list

            processed_names = np.char.strip(np.char.translate(
                np.char.lower(names), {
                    ord("."): "",
                    ord("'"): "",
                    ord(","): "",
                    ord("&"): "",
                    ord(")"): "",
                    ord("("): "",
                    ord("\\"): "",
                    ord("`"): "",
                    ord("-"): " ",
                    ord("@"): "a",
                }))
            name_lengths = np.vectorize(len)(processed_names)
            name_char_dict = dict(zip(self._name_char_list, range(len(self._name_char_list))))
            # country_dict = dict(zip(self._country_list, range(len(self._country_list))))
            name_char_indices = (
                np.fromiter((name_char_dict[x] for x in s),
                            dtype=np.int32) for s in processed_names)
            # country_indices = np.vectorize(lambda s: np.int32(country_dict[s]))(self._country_list)
            max_length = name_lengths.max(axis=0)
            padded_name_char_indices = (np.pad(x, (0, max_length - len(x)),
                                               mode="constant",
                                               constant_values=(0, 0)) for x in name_char_indices)
            name_char_matrix = np.row_stack(padded_name_char_indices)

            model = self.model
            inference = sess.run(
                model.outs.inference,
                feed_dict={
                    model.ins.input_name_chars: name_char_matrix,
                    model.ins.input_name_lengths: name_lengths,
                })
            return inference

        if modelfile is not None:
            with tf.Session(graph=self._graph) as sess:
                self.train(sess, None, 0, modelfile=modelfile)
                return subfunc(sess)
        else:
            return subfunc(sess)

    def validate(self,
                 name_matrix, name_lengths, country_vector,
                 sess=None,
                 name_char_list=None,
                 country_list=None,
                 num_guesses=5,
                 modelfile=None):

        # either sess or modelfile (but not both) are provided.
        assert sess is None or modelfile is None
        assert sess is not None or modelfile is not None

        def subfunc(sess):
            # validation purpose if not None.
            if name_char_list is not None:
                assert self._name_char_list == name_char_list
            if country_list is not None:
                assert self._country_list == country_list

            model = self.model

            intopk = tf.nn.in_top_k(model.outs.inference, model.ins.input_countries, k=num_guesses)

            result = sess.run(
                intopk,
                feed_dict={
                    model.ins.input_name_chars: name_matrix,
                    model.ins.input_name_lengths: name_lengths,
                    model.ins.input_countries: country_vector,
                })

            return result

        if modelfile is not None:
            with tf.Session(graph=self._graph) as sess:
                self.train(sess, None, 0, modelfile=modelfile)
                return subfunc(sess)
        else:
            return subfunc(sess)

    def country_weights_biases(self, sess):
        country_weights, country_biases = sess.run([self.model.outs.country_weights,
                                                    self.model.outs.country_biases])
        return collections.namedtuple("country", ["w", "b"])(country_weights, country_biases)

    def train(self,
              sess,
              batch_generator,
              num_optimization_steps,
              name_char_list=None,
              country_list=None,
              optimizer=tf.train.AdamOptimizer(learning_rate=0.001),
              modelfile=None,
              logdir="/tmp/logdir"):
        """
        :param batch_generator: a function that maps a positive integer for the batch size to a tuple of
                        (a numpy matrix of (batch size, time length) for names,
                         time length for each row in name matrix,
                         a numpy array for country of size batch_size).
                        Note components in name matrix are padded with zeroes if they are after
                        the end of each name.
        :param num_optimization_steps:
        :param logdir:
        :return:
        """

        if self._name_char_list is None:
            self._name_char_list = name_char_list
        else:
            assert self._name_char_list == name_char_list

        if self._country_list is None:
            self._country_list = country_list
        else:
            assert self._country_list == country_list

        model = self.model
        tf.scalar_summary("loss", model.outs.loss)

        ema = tf.train.ExponentialMovingAverage(decay=0.99)
        update_loss_ema = ema.apply([model.outs.loss])
        loss_ema_op = ema.average(model.outs.loss)
        tf.scalar_summary("loss_ema", loss_ema_op)

        summary_op = tf.merge_all_summaries()
        summary_writer = tf.train.SummaryWriter(logdir=logdir, graph=self._graph)

        with tf.variable_scope("optimizer"):
            optimize_op = optimizer.minimize(model.outs.loss)

        saver = tf.train.Saver()

        if modelfile is None:
            self.logger.debug("initializing all tensorflow variables...")
            sess.run(tf.initialize_all_variables())
        else:
            self.logger.debug("loading model file {} to initialize variables...".format(modelfile))
            saver.restore(sess, modelfile)

        for step in range(num_optimization_steps):
            name_matrix, name_lengths, country_vector = next(batch_generator)
            loss_ema, summary, _, _ = sess.run(
                [loss_ema_op, summary_op, optimize_op, update_loss_ema],
                feed_dict={
                    model.ins.input_name_chars: name_matrix,
                    model.ins.input_name_lengths: name_lengths,
                    model.ins.input_countries: country_vector,
                }
            )
            if step % 1000 == 0:
                summary_writer.add_summary(summary, global_step=step)
                ckptname = os.path.join(logdir, "model-snapshot")
                self.logger.info("saving model snapshot in {}-{}".format(ckptname, step))
                saver.save(sess, ckptname, global_step=step)
            print("\rStep {}. Loss EMA: {}.".format(step, loss_ema), end="")

        print("")


def check_float(x):
    try:
        v = float(x)
        return True
    except ValueError:
        return False

    return False
