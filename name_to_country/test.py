from name_to_country.olympic_athletes import OlympicAthletes
from name_to_country.learn_rnn import LearnRnn
from name_to_country.util import plot_country_embeddings
from name_to_country.editdist import EditDistance

import logging
import tensorflow as tf
import os
import sys

# use logging.DEBUG instead of logging.INFO to see more detailed messages.
logging.basicConfig(level=logging.INFO)

logger = logging.getLogger("test")

def prepare_data():
    """ prepare olympic athelete database. this is used by other run_* methods."""
    lockfile = "/tmp/olympic_athletes_row_indices.json"
    # if a lock file exists, use the row indices in their to choose train and test sampled.
    # otherwise, create train and test samples randomly and save the indices
    # in the lock file, to be used later.
    if os.path.exists(lockfile):
        partition = None
    else:
        partition = {"train": 0.8, "test": 0.2}

    return OlympicAthletes(partition=partition, lockfile=lockfile)

def run_rnn_train(num_steps=100, modelfile=None):
    """ rnn train example """
    logger.info("rnn training starts.")
    logdir = "/tmp/logdir"
    o = prepare_data()
    learner = LearnRnn(
        num_rnn_cells=[64,64],
        num_output_layers=[128],
        char_embedding_dims=[27,64],
        country_embedding_dims=[228,128],
        output_keep_prob=0.5,
    )
    with tf.Session() as sess:
        learner.train(sess, o.batch_generator("train", 32),
                      num_steps,
                      o.name_char_list,
                      o.country_list,
                      modelfile=modelfile,
                      logdir=logdir)
        plot_country_embeddings(learner.country_weights_biases(sess).w, o.country_list,
                                plot_only=100, filename="countries.png")
    logger.info("rnn training finished. check model files under {}".format(logdir))
    logger.info("the first 100 frequent countries are mapped in a 2 dimentional plane in ./countries.png")

    return o, learner

def run_rnn_valid(modelfile, batch_size, num_guesses=5):
    """
    Print validation result over the test set of size `batch_size`.
    Use `batch_size=None` to use all test samples.
    """

    logger.info("rnn validation stars.")
    o = prepare_data()
    learner = LearnRnn(
        num_rnn_cells=[64, 64],
        num_output_layers=[128],
        char_embedding_dims=[27, 64],
        country_embedding_dims=[228, 128],
        output_keep_prob=1.0,
        graph=tf.Graph(),
    )
    valid_result = learner.validate(
                        *o.generate_batch("test", batch_size), modelfile=modelfile,
                        num_guesses=num_guesses)
    correct_ratio = sum(valid_result) / len(valid_result)
    logger.info("validation run finishes. correct ratio is {:.5f}".format(correct_ratio))
    logger.info("test sample size={}, number of correct samples={}".format(
        len(valid_result), sum(valid_result)))

def run_rnn_inference(modelfile, names, num_guesses=5):
    """ infer the `num_guesses` best country guesses for each name in `names` """
    o = prepare_data()
    learner = LearnRnn(
        num_rnn_cells=[64, 64],
        num_output_layers=[128],
        char_embedding_dims=[27, 64],
        country_embedding_dims=[228, 128],
        output_keep_prob=1.0,
        graph=tf.Graph(),
    )
    softmaxes = learner.inference(names, modelfile=modelfile,
                                  name_char_list=o.name_char_list,
                                  country_list=o.country_list)
    bestguesses = dict(zip(names, learner.best_guess(softmaxes, num_guesses=num_guesses)))
    logger.info("best guesses are {}".format(bestguesses))

def run_editdist(batch_size):
    o = prepare_data()
    valid_names, valid_name_lengths, valid_countries = o.generate_batch("test", batch_size)
    ed = EditDistance(o.name, o.country)
    # pair for (name vector, country vector).
    train_batch_pair = o.string_generate_batch("train", None)
    test_batch_pair = o.string_generate_batch("test", batch_size)
    valid_result = ed.validate(train_batch_pair, test_batch_pair)
    correct_ratio = sum(valid_result) / len(valid_result)
    logger.info("validation run finishes. correct ratio is {:.5f}".format(correct_ratio))
    logger.info("test sample size={}, number of correct samples={}".format(
        len(valid_result), sum(valid_result)))

if __name__ == "__main__":
    if len(sys.argv) > 1: num_steps = int(sys.argv[1])
    else: num_steps = 1000
    logger.info("Running {} training steps... (provide a command line argument to override)".format(num_steps))
    run_rnn_train(num_steps=num_steps+1, modelfile=None)
    logger.info("Validating the training result using the test samples...")
    # in learn_rnn.py, training snapshot is saved every 1000 steps.
    last_step = 1000 * (num_steps // 1000)
    modelfile = "/tmp/logdir/model-snapshot-{}".format(last_step)
    logger.info("Model file is {}".format(modelfile))
    run_rnn_valid(modelfile=modelfile, batch_size=None, num_guesses=5)
    logger.info("If we use enough training steps, the accuracy is around 76%.")
    logger.info("Test the inference result for Neil Young, Ma YoYo and Joseph Polchinski. " +\
                "Note, in the training database, Korean/Chinese/Japan names are last name + first name.")
    run_rnn_inference(modelfile=modelfile,
                      names=["Neil Young", "Ma YoYo", "Joseph Polchinski"],
                      num_guesses=5)
    # edit distance (Levenshtein distance) based method gives an accuracy of about 68%.
    # since it takes a long time, let me comment it for now.
    # run_editdist(None)
