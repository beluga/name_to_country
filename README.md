## Introduction

This code guesses countries based on people's names.
It uses tensorflow and is based on the recurrent neural network.
The code reads names character by character, runs the RNN and calculates
the soft max for each country.

The training set is the olympic athlete database.
Note that in the database, the natural order of last/first names are preserved
in some countries.
For example, a person in Korea with first name "Chang-Soon" and last name "Park"
is stored as "Park Chang-Soon" in the database.

The code sets aside 20% of the database for validation, and uses 80% for training.
In the validation process, the best 5 guesses are matched with the true country.

There is another implementation based on edit distance (Levenshtein).
A naive implementation based on finding the best guesses using distance
between names gives about 68% accuracy.
The RNN based algorithm gives 76~78% of accuracy after 200k or so steps
of trainings. No fine tuning has been performed so
the result can be improved in the future.

## Getting Started

To test, create a directory to clone the repository.

```
git clone https://gitlab.com/beluga/name_to_country.git
```

In the top directory of this codebase,

```
PYTHONPATH=. python name_to_country/test.py
```

Add a command line argument such as 200000, to set the number of training steps.
By default, it is 1000.

Note that you might need to install some packages, including,

```
tensorflow, numpy, pandas, editdistance, etc.
```

`conda` might be a good tool to install all dependencies,
but other options (e.g. `pip`) should be fine too.
The code is written for Python 3, not 2.

## Example Results

During training, you can check the progress using `tensorboard`.

```
tensorboard --logdir /tmp/logdir/
```

![Alt text](doc/loss.png)

After the training, you can check the country embedding for the most frequent 100 countries in a plane.

![Alt text](doc/countries.png)

During the `test.py` script running, you can see some inference results for the best 5 guesses of some names.

```
INFO:test:best guesses are
{'Neil Young': ['United States', 'Great Britain', 'Australia', 'Canada', 'New Zealand'],
'Joseph Polchinski': ['Switzerland', 'United States', 'France', 'Canada', 'Germany'],
'Ma YoYo': ['China', 'Japan', 'South Korea', 'Singapore', 'North Korea']}
```
